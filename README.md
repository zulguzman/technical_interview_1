# Purpose

The purpose of this technical interview is to assess your coding aptitude. 

There are two projects for you to choose from.
If you successfully complete your chosen project before the end of the allotted time, begin
working on the other project. There is an optional stretch goal of putting the two projects 
together into a single program. We do not expect you to complete both projects in the 
alloted time.


## Project A
This part consists of parsing a JSON file into a CSV file. In the resources directory you will find some example JSON
documents. The JSON input file names will be of the form {id}.json, and its output should be named {id}.csv. The CSV
should include column headings of the same name as the JSON keys.
-----------------------------------------------------------------  


## Project B
This part consists of making calls to two web services, parsing the response, and aggregating the results.
By way of background, the following web services provide data about item usage in the library. The first URL
returns "use_item" data broken down by year (use_item data is the number of usages a book has in the library 
without being checked out). The second URL has information about the number of checkouts for the item. Both
of these services will return JSON.

https://search.lib.byu.edu/dacapo/statistics?itemId={itemId}&action=use_item&aggregation=yearly
https://search.lib.byu.edu/dacapo/statistics?itemId={itemId}&action=charge&aggregation=yearly

Your task is to combine the information from both web services and output it as JSON (print to screen, write to file, etc. it's your choice).
To construct the combined JSON, rename the "values" in the use_item service to "uses", and the "values" in the charge service to "checkouts".

In the resources directory you will find some sample outputs that you may check against.
i----------------------------------------------------------------

## Stretch Goal
The stretch goal is to combine Part A and Part B into a single project. You will make the two web service calls for 
an arbitrary item ID and aggregate the data, then parse the data into a CSV file per the requirements in Part B. As
an additional requirement, do not save the JSON as a file and run the file through Part B, but fully integrate Part A
into Part B with the only file being created being the final CSV file.


Item IDs:
31197001029161
31197227271084
31197231672921



